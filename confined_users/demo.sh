#!/usr/bin/bash

. ../functions.sh

if ! rpm -q pv; then
    dnf install pv
fi

########################
# include the magic
########################
. demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=80

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="# "

useradd -Z staff_u localuser >& /dev/null

mkdir -p ~localuser/.ssh
chmod 700 ~localuser/.ssh

chmod 600 id_rsa

cat id_rsa.pub > ~localuser/.ssh/authorized_keys
chmod 600 ~localuser/.ssh/authorized_keys

chown -R localuser: ~localuser/

ssh-keyscan localhost > ~/.ssh/known_hosts 2> /dev/null
chmod 600 ~/.ssh/known_hosts

cat > /etc/sudoers.d/localuser <<EOF
localuser        ALL=(ALL)       NOPASSWD: ALL
EOF

clear

DEMO_PROMPT="localuser $ "

note "localuser stole root password"

p "su -"

echo -n "Password: "
echo

DEMO_PROMPT="# "
p "whoami"
fakesu whoami

note "Looks promising"

p "ls -l /root/"
fakesu ls -l /root

note "What?"

p "cat /root/.bashrc"

fakesu cat /root/.bashrc

note "Oh my, lets try to remove /etc/passwd"
p "rm /etc/passwd"
fakesu rm /etc/passwd

note "Am I still root?"
p "whoami"
fakesu whoami

note "Grrrrr"

note "What has just happened?"

p "id -Z"
fakesu id -Z

note "He's root but his shell still runs under confined user. The real root looks like this:"
DEMO_PROMPT="real root# "
pe "id -Z"


sleep 15
userdel localuser
