#!/usr/bin/bash

. ../functions.sh

if ! rpm -q pv; then
    dnf install pv
fi

cp update-etc-suspicious-file /usr/local/bin

cp suspicious.service suspicious.timer /etc/systemd/system
systemctl daemon-reload
systemctl start suspicious.timer

auditctl -D

########################
# include the magic
########################
. demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=80

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="# "


# hide the evidence
clear

note "something created a suspicious file"
pe "cat /etc/suspicious"

note "lets remove it"
pe "rm /etc/suspicious"

note "but it's back!!!"
pe "cat /etc/suspicious"

note "there's nothing suspicious"

p "ps axf"
echo "......."
ps axf | tail -n 15

note "we need to add a watch on the file"
pe "auditctl -a exit,always -F path=/etc/suspicious -p w -k suspicious"

pe "START=\`date +%H:%M:%S\`"

note "and try it again"
pe "rm /etc/suspicious"

pe "cat /etc/suspicious"

pe "ausearch -i -k suspicious -ts \$START"

note "here it is"

pe "cat /usr/local/bin/update-etc-suspicious-file"

pe "grep -r /usr/local/bin/update-etc-suspicious-file /usr/lib/systemd /etc/systemd"

systemctl stop suspicious.timer
rm -f /etc/systemd/system/suspicious.* /etc/suspicious /usr/local/bin/update-etc-suspicious-file
systemctl daemon-reload
