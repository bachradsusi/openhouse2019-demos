#!/usr/bin/bash

. ../functions.sh

if ! rpm -q pv; then
    dnf install pv
fi

if ! rpm -q postfix > /dev/null; then
    dnf install postfix
fi

if ! rpm -q mailx > /dev/null; then
    dnf install mailx
fi

if ! rpm -q gnupg2 > /dev/null; then
    dnf install gnupg2
fi

systemctl start postfix

adduser jane
adduser tony

echo > /var/spool/mail/jane
echo > /var/spool/mail/tony

########################
# include the magic
########################
. demo-magic.sh

########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=80

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="# "


clear

note "Tony sends an email to Jane"

DEMO_PROMPT="${GREEN} tony $ "
p "mail -r 'tony@localhost' -s 'Secret number' jane@localhost <<EOF
Hi Jane,

this is secret number 123456.

Please do not tell anyone!
EOF
"
mail -r 'tony@localhost' -s 'Secret number' jane@localhost <<EOF
Hi Jane,

this is secret number 123456.

Please do not tell anyone!
EOF

note "But there's an evil administrator in the middle"

DEMO_PROMPT="${RED} evil admin # "
pe "cat /var/spool/mail/jane"

pe "sed -i 's/secret number 123456/secret number 654321/' /var/spool/mail/jane"

note "${RED}MUHAHAHAHA${COLOR_RESET}"

DEMO_PROMPT="${GREEN} jane $ "

note "Jane reads the email"

p "mail"
echo |  mail -u jane

export GNUPGHOME=`pwd`/.gnupg-jane
note "But the secret number is wrong. Jane smells a rat and uses gnupg2"
p "cat <<EOF | gpg2 -eas -r 'tony@localhost' | \\
       mail -r 'jane@localhost' -s 'Re: Secret number' tony@localhost
Hello Tony,

something is wrong, the number does not work.

Lets use cryptography!
EOF"

cat <<EOF | gpg2 -eas -r 'tony@localhost' | \
    mail -r 'jane@localhost' -s 'Re: Secret number' tony@localhost
Hello Tony,

something is wrong, the number does not work.

Lets use cryptography!
EOF

DEMO_PROMPT="${RED} evil admin # "
note "The evil administrator can't read this email"
pe "cat /var/spool/mail/tony"

export GNUPGHOME=`pwd`/.gnupg-tony
DEMO_PROMPT="${GREEN} tony $ "
note "Tony received a signed encrypted message"
p "mail"
echo | mail -u tony

p "mail | gpg2 -d"
echo | mail -u tony | gpg2 -d

userdel jane
userdel tony
